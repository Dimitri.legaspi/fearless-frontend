import React, {useEffect, useState} from 'react';

function ConferenceForm(){
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [description, setDescription] = useState('');
    const [presentations, setPresentation] = useState('');
    const [attendees, setAttendee] = useState('');
    const [location, setLocation] = useState('');

    const locationChanged = (event) => {
        setLocation(event.target.value);
    }
    const nameChanged = (event) => {
        setName(event.target.value);
    }
    const startDateChanged = (event) => {
        setStartDate(event.target.value);
    }
    const endDateChanged = (event) => {
        setEndDate(event.target.value);
    }
    const descriptionChanged = (event) => {
        setDescription(event.target.value);
    }
    const presentationsChanged = (event) => {
        setPresentation(event.target.value);
    }
    const attendeesChanged = (event) => {
        setAttendee(event.target.value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = attendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setPresentation('');
            setAttendee('');
            setLocation('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);




    return (
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form id="create-conference-form" onSubmit={handleSubmit}>
                  <div className="form-floating mb-3">
                    <input placeholder="Name" required type="text" name="name" id="name" className="form-control" onChange={nameChanged} value={name}/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" onChange={startDateChanged} value={startDate}/>
                    <label htmlFor="starts">Starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" onChange={endDateChanged} value={endDate}/>
                    <label htmlFor="ends">Ends</label>
                  </div>
                  <div className= "mb-3">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea className="form-control" name="description" id="description" rows="3" onChange={descriptionChanged} value={description}></textarea>
                  </div>
                   <div className="form-floating mb-3">
                    <input placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" onChange={presentationsChanged} value={presentations}/>
                    <label htmlFor="max_presentations">Maximum presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" onChange={attendeesChanged} value={attendees}/>
                    <label htmlFor="max_attendees">Maximum attendees</label>
                  </div>
                  <div className="mb-3">
                    <select required  name="location" id="location" className="form-select" onChange={locationChanged} value={location}>
                      <option value="">Choose a location</option>
                      {locations.map(location => {
                        return (
                            <option value={location.id} key={location.id}>
                                {location.name}
                            </option>
                        );
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
    );
}

export default ConferenceForm;
import React, {useEffect, useState} from "react";
function PresentationForm () {
    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [company, setCompany] = useState("");
    const [title, setTitle] = useState("");
    const [synopsis, setSynopsis] = useState("");
    const [conference, setConference] = useState("");

    const changeName = (event) => {
        setName(event.target.value);
    }
    const changeEmail = (event) => {
        setEmail(event.target.value);
    }
    const changeCompany = (event) => {
        setCompany(event.target.value);
    }
    const changeTitle = (event) => {
        setTitle(event.target.value);
    }
    const changeSynopsis = (event) => {
        setSynopsis(event.target.value);
    }
    const changeConference = (event) => {
        setConference(event.target.value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.presenter_name = name;
        data.presenter_email = email; 
        data.company_name = company;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;
        const conferenceId = data.conference;
        
        const url = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchOptions);
        if(response.ok){
            setName('');
            setEmail('');
            setCompany('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }

        }


    return (
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form id="create-presentation-form" onSubmit={handleSubmit}>
                  <div className="form-floating mb-3">
                    <input placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" className="form-control" onChange={changeName} value={name} />
                    <label htmlFor="presenter_name">Presenter name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Presenter Email" required type="email" name="presenter_email" id="presenter_email" className="form-control" onChange={changeEmail} value={email}/>
                    <label htmlFor="presenter_email">Presenter email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Company Name" required type="text" name="company_name" id="company_name" className="form-control" onChange={changeCompany} value={company}/>
                    <label htmlFor="company_name">Company name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Title" required type="text" name="title" id="title" className="form-control" onChange={changeTitle} value={title}/>
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className= "mb-3">
                    <label htmlFor="synopsis" className="form-label">Synopsis</label>
                    <textarea className="form-control" name="synopsis" id="synopsis" rows="3" onChange={changeSynopsis} value={synopsis}></textarea>
                  </div>
                  <div className="mb-3">
                    <select required  name="conference" id="conference" className="form-select" onChange={changeConference} value={conference}>
                      <option value="">Choose a conference</option>
                      {conferences.map(conference => {
                        return (
                            <option value={conference.id} key={conference.id}>{conference.name}</option>
                        );
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
    );
}

export default PresentationForm;
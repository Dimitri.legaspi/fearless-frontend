import './App.css';
import React from 'react';
import Nav from './Nav';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConfernceForm';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import {BrowserRouter, Routes, Route} from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav className="Navbar" />
    <Routes>
      <Route index element={<MainPage />} />
        <Route path="locations">
            <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
            <Route path="new" element={ <ConferenceForm />} />
        </Route>
        <Route path="presentations">
            <Route path="new" element={ <PresentationForm />} />
        </Route>
        <Route path="attendees">
            <Route path="new" element={ <AttendConferenceForm />} />
            <Route path="" element={<AttendeesList attendees={props.attendees} />} />
        </Route>
    </Routes>
  </BrowserRouter>
  );
}

export default App;